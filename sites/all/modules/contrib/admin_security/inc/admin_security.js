(function ($) {
  Drupal.behaviors.adminSecurity = {
    attach: function (context, settings) {
      $('form#admin-security-form input#edit-enabled').click(function () {
        var version = parseFloat($.fn.jquery);
        if ($(this).is(':checked')) {
          if (version >= 1.6) {
            $('form#admin-security-form input#edit-login-blocking-enabled').prop('disabled', false);
          } else {
            $('form#admin-security-form input#edit-login-blocking-enabled').removeAttr('disabled');
          }
        } else {
          if (version >= 1.6) {
            $('form#admin-security-form input#edit-login-blocking-enabled').prop('checked', false);
            $('form#admin-security-form input#edit-login-blocking-enabled').prop('disabled', true);
          } else {
            $('form#admin-security-form input#edit-login-blocking-enabled').attr('checked', false);
            $('form#admin-security-form input#edit-login-blocking-enabled').attr('disabled', 'disabled');
          }

        }
      });
    }
  };
})(jQuery);