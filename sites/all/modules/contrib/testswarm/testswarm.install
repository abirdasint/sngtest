<?php

/**
 * @file
 *   TestSwarm module install/schema hooks.
 */

/**
 * Implements hook_schema().
 */
function testswarm_schema() {
  $schema = array();

  $schema['testswarm_test'] = array(
    'description' => 'Table for tests.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'caller' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'theme' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'githash' => array(
        'type' => 'varchar',
        'length' => 40,
        'not null' => FALSE,
      ),
      'useragent' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'total' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'passed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'failed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'runtime' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'karma' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'caller' => array('caller'),
      'githash' => array('githash'),
      'theme' => array('theme'),
      'karma' => array('karma'),
    ),
    'primary key' => array('id'),
  );

  $schema['testswarm_test_run'] = array(
    'description' => 'Table for storing test runs.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'qt_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'module' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'total' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'passed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'failed' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['testswarm_test_run_detail'] = array(
    'description' => 'Table for storing test run details.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
      ),
      'tri' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'result' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'message' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'actual' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
      'expected' => array(
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Add githash column.
 */
function testswarm_update_7001() {
  $githash = array(
    'githash' => array(
      'type' => 'varchar',
      'length' => 40,
      'not null' => FALSE,
    ),
  );

  db_add_field('testswarm_test', 'githash', $githash['githash']);
}

/**
 * Add theme column.
 */
function testswarm_update_7002() {
  $theme = array(
    'theme' => array(
      'type' => 'varchar',
      'length' => 150,
      'not null' => FALSE,
    ),
  );

  db_add_field('testswarm_test', 'theme', $theme['theme']);
  db_query("UPDATE {testswarm_test} set theme = 'bartik'");
}

/**
 * Add karma column.
 */
function testswarm_update_7003() {
  $karma = array(
    'karma' => array(
      'type' => 'varchar',
      'length' => 50,
      'not null' => FALSE,
    ),
  );

  db_add_field('testswarm_test', 'karma', $karma['karma']);
  db_query("UPDATE {testswarm_test} set karma = 'jane doe'");
}

/**
 * Add indexes.
 */
function testswarm_update_7004() {
  db_add_index('testswarm_test', 'caller', array('caller'));
  db_add_index('testswarm_test', 'githash', array('githash'));
  db_add_index('testswarm_test', 'theme', array('theme'));
  db_add_index('testswarm_test', 'karma', array('karma'));
}
