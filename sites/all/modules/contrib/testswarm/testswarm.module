<?php

/**
 * @file
 *   TestSwarm module.
 *   @TODO: move pages to testswarm.pages.inc
 */

/**
 * Implements hook_permission().
 */
function testswarm_permission() {
  return array(
    'administer testswarm tests' => array(
      'title' => t('administer testswarm tests'),
      'description' => t('Manage and run automated testing.'),
      'restrict access' => TRUE,
    ),
    'run testswarm tests' => array(
      'title' => t('run testswarm tests'),
      'description' => t('Run automated testing.'),
      'restrict access' => TRUE,
    ),
    'administer testswarm settings' => array(
      'title' => t('administer testswarm settings'),
      'description' => t('Administer TestSwarm settings'),
      'restrict access' => TRUE,
    ),
    'is allowed to submit forms' => array(
      'title' => t('is allowed to submit forms'),
      'description' => t('Is allowed to submit forms, if not granted all submit handlers are disabled.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function testswarm_menu() {
  $items['testswarm-tests'] = array(
    'title' => 'Overview of all tests',
    'description' => 'All tests and results',
    'page callback' => 'testswarm_tests',
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );
  $items['testswarm-tests/%'] = array(
    'title' => 'Overview of all tests',
    'description' => 'All tests and results',
    'page callback' => 'testswarm_tests',
    'page arguments' => array(1, 2, 3),
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );
  $items['testswarm-tests/clear/%'] = array(
    'title' => 'Clear TestSwarm tests details',
    'description' => 'Clear TestSwarm tests details',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testswarm_clear_test_details', 2),
    'access arguments' => array('administer testswarm tests'),
    'file' => 'testswarm.admin.inc',
  );
  $items['testswarm-tests/detail/%'] = array(
    'title' => 'TestSwarm tests details',
    'description' => 'TestSwarm tests details',
    'page callback' => 'testswarm_test_details',
    'page arguments' => array(2),
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );
  $items['testswarm-tests/detail/%/%'] = array(
    'title' => 'TestSwarm tests details',
    'description' => 'TestSwarm tests details',
    'page callback' => 'testswarm_test_details',
    'page arguments' => array(2, 3),
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );
  $items['testswarm-tests/detail/%/tests/%'] = array(
    'title' => 'TestSwarm tests details',
    'description' => 'TestSwarm tests details',
    'page callback' => 'testswarm_test_details_tests',
    'page arguments' => array(2, 4),
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );
  $items['testswarm-tests/detail/%/hash/%'] = array(
    'title' => 'TestSwarm tests details',
    'description' => 'TestSwarm tests details',
    'page callback' => 'testswarm_test_details_hash',
    'page arguments' => array(2, 4),
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );
  $items['testswarm-browser-tests'] = array(
    'title' => 'TestSwarm tests for the current browser',
    'description' => 'All tests for the current browser',
    'page callback' => 'testswarm_browser_tests',
    'access arguments' => array('run testswarm tests'),
  );

  $items['testswarm-run-all-tests'] = array(
    'title' => 'Run all TestSwarm tests',
    'description' => 'Run all TestSwarm tests.',
    'page callback' => 'testswarm_run_all_tests',
    'access arguments' => array('run testswarm tests'),
  );
  $items['testswarm-run-a-test/%'] = array(
    'title' => 'Run a TestSwarm test',
    'description' => 'Run a test.',
    'page callback' => 'testswarm_run_a_test',
    'page arguments' => array(1),
    'access arguments' => array('run testswarm tests'),
  );
  $items['testswarm-test-done'] = array(
    'title' => 'TestSwarm javaScript post back',
    'page callback' => 'testswarm_test_done',
    'type' => MENU_CALLBACK,
    'access arguments' => array('run testswarm tests'),
  );

  $items['testswarm-karma'] = array(
    'title' => 'Set karma cookie',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testswarm_set_karma_form'),
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );
  $items['testswarm-set-karma/%'] = array(
    'title' => 'Set karma cookie',
    'page callback' => 'testswarm_set_karma',
    'page arguments' => array(1),
    'access arguments' => array('run testswarm tests'),
  );

  $items['browserstack-status'] = array(
    'title' => 'Browserstack status',
    'description' => 'Overview of all workers on browserstack.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testswarm_browserstack_status'),
    'access arguments' => array('administer testswarm settings'),
    'file' => 'testswarm.pages.inc',
  );

  $items['admin/config/development/testswarm'] = array(
    'title' => 'TestSwarm',
    'description' => 'Administer TestSwarm settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testswarm_admin_settings_form'),
    'file' => 'testswarm.admin.inc',
    'access arguments' => array('administer testswarm settings'),
  );

  $items['testswarm-framed/%/%'] = array(
    'title' => 'Run a framed test',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('testswarm_framed_form', 1, 2),
    'access arguments' => array('run testswarm tests'),
    'file' => 'testswarm.pages.inc',
  );

  return $items;
}

/**
 * Run all test the current user can access.
 */
function testswarm_run_all_tests($module = '') {
  $tests = testswarm_defined_tests_access($module);
  $enabled_themes = testswarm_themes_to_test();

  $tests_to_run = array();
  foreach($enabled_themes as $enabled_theme) {
    $tests_to_run[$enabled_theme] = $tests;
  }

  $_SESSION['testswarm_tests'] = $tests_to_run;
  testswarm_run_a_test('', FALSE);
}

/**
 * Run a specific test or use the first one in the session.
 */
function testswarm_run_a_test($caller = '', $allowredirect = TRUE) {
  $tests = testswarm_defined_tests_access();
  if (!empty($tests)) {
    $theme_to_test = '';
    if (empty($caller) && isset($_SESSION['testswarm_tests']) && !empty($_SESSION['testswarm_tests'])) {
      $theme_to_test = array_shift(array_keys($_SESSION['testswarm_tests']));
      $tests = $_SESSION['testswarm_tests'];
      $test = array_shift($tests[$theme_to_test]);
      if (empty($tests[$theme_to_test])) {
        unset($tests[$theme_to_test]);
      }
      $_SESSION['testswarm_tests'] = $tests;
    }
    else {
      $test = $tests[$caller];
    }

    if ($test) {
      $query = $test['query'];
      if (!is_array($query)) {
        $query = (array)$query;
      }
      $query += array('testswarm-test' => $test['caller']);
      if (isset($_GET['debug'])) {
        $query += array('debug' => $_GET['debug']);
      }
      if (!empty($theme_to_test)) {
        $query += array('testswarm-theme' => $theme_to_test);
      }
      elseif (isset($_GET['testswarm-theme'])) {
        $query += array('testswarm-theme' => $_GET['testswarm-theme']);
      }
      if ($allowredirect) {
        if (isset($_GET['destination'])) {
          $query += array('destination' => $_GET['destination']);
        }
        unset($_GET['destination']);
      }
      drupal_goto($test['path'], array('query' => $query));
    }
  }
}

/**
 * Alter the page output if a test is requested.
 */
function testswarm_page_alter(&$page) {
  $test = '';
  if (isset($_GET['testswarm-test'])) {
    $caller = $_GET['testswarm-test'];
    $tests = testswarm_defined_tests();
    if (array_key_exists($caller, $tests)) {
      $test = $tests[$caller];
    }
  }

  if ($test) {
    $karma = '';
    if (isset($_COOKIE['karma']) && !empty($_COOKIE['karma'])) {
      $karma = $_COOKIE['karma'];
    }
    
    $page['content']['system_main']['#prefix'] = '<h1 id="qunit-header">QUnit Tests</h1>
      <h2 id="qunit-banner"></h2>
      <div id="qunit-testrunner-toolbar"></div>
      <h2 id="qunit-userAgent"></h2>
      <ol id="qunit-tests"></ol>
      <div id="xtestswarm-fixture">';
    $page['content']['system_main']['#suffix'] = '</div>';

    if (!isset($page['content']['system_main']['#attached']['css'])) {
      $page['content']['system_main']['#attached']['css'] = array();
    }
    $page['content']['system_main']['#attached']['css'] += array(
      drupal_get_path('module', 'testswarm') . '/qunit/qunit.css' => array(),
    );

    if (!isset($page['content']['system_main']['#attached']['js'])) {
      $page['content']['system_main']['#attached']['js'] = array();
    }
    $page['content']['system_main']['#attached']['js'] += array(
      drupal_get_path('module', 'testswarm') . '/qunit/qunit.js' => array('weight' => JS_LIBRARY),
      drupal_get_path('module', 'testswarm') . '/testswarm.js' => array('weight' => JS_LIBRARY),
      drupal_get_path('module', 'testswarm') . '/testswarm.admin.js' => array('weight' => JS_LIBRARY),
    );
    $page['content']['system_main']['#attached']['js'] += $test['js'];
    $page['content']['system_main']['#attached']['js'][] = array(
      'data' => array(
        'testswarm' => array(
          'caller' => $test['caller'],
          'theme' => check_plain($_GET['testswarm-theme']),
          'karma' => $karma,
          'token' => drupal_get_token($test['caller'], TRUE),
          'debug' => (isset($_GET['debug']) ? 'on' : 'off'),
          'destination' => (isset($_GET['destination']) ? '/' . $_GET['destination'] : '/testswarm-browser-tests'),
        ),
      ),
      'type' => 'setting',
    );
  }
}

/**
 * Alter all forms to disable submit and validate handlers.
 * @TODO: add permission so more people can still use submit.
 */
function testswarm_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  if ($user->uid != 1 && $form_id !== 'user_login' && $form_id !== 'testswarm_set_karma_form') {
    if (!user_access('is allowed to submit forms')) {
      _testswarm_disable_submit($form);
    }
  }
}

/**
 * Disable all submit and validate handlers of a form.
 */
function _testswarm_disable_submit(&$form) {
  if (is_array($form)) {
    foreach ($form as $key => &$part) {
      if ($key === '#type' && $part === 'submit' && !isset($form['#submit'])) {
        $form['#submit'] = array('testswarm_empty_submit');
      }
      if ($key === '#type' && $part === 'submit' && !isset($form['#validate'])) {
        $form['#validate'] = array('testswarm_empty_submit');
      }

      if ($key === '#submit' || $key === '#validate') {
        if (is_array($part)) {
          $part = array('testswarm_empty_submit');
        }
        else {
          $part = 'testswarm_empty_submit';
        }
      }
      elseif (is_array($part)) {
        _testswarm_disable_submit($part);
      }
    }
  }
}

/**
 * Empty handler for submit and validate.
 */
function testswarm_empty_submit() {
}

/**
 * Process the results of the test.
 */
function testswarm_test_done() {
  if (isset($_REQUEST['caller']) && isset($_REQUEST['token'])) {
    if (drupal_valid_token($_REQUEST['token'], $_REQUEST['caller'], TRUE)) {
      global $user;
      $qt_id = db_insert('testswarm_test')
        ->fields(array(
          'caller' => $_REQUEST['caller'],
          'githash' => variable_get('testswarm_githash', ''),
          'theme' => $_REQUEST['theme'],
          'karma' => drupal_substr($_REQUEST['karma'], 0, 50),
          'useragent' => $_SERVER['HTTP_USER_AGENT'],
          'total' => $_REQUEST['info']['total'],
          'passed' => $_REQUEST['info']['passed'],
          'failed' => $_REQUEST['info']['failed'],
          'runtime' => $_REQUEST['info']['runtime'],
          'uid' => $user->uid,
          'timestamp' => REQUEST_TIME,
        ))
        ->execute();
      if (isset($_REQUEST['tests'])) {
        foreach ($_REQUEST['tests'] as $test) {
          $test_run_id = db_insert('testswarm_test_run')
            ->fields(array(
              'qt_id' => $qt_id,
              'module' => $test['module'],
              'name' => $test['name'],
              'total' => $test['total'],
              'passed' => $test['passed'],
              'failed' => $test['failed'],
            ))
            ->execute();
          if (isset($_REQUEST['log'][$test['module']][$test['name']])) {
            foreach ($_REQUEST['log'][$test['module']][$test['name']] as $testdetail) {
              db_insert('testswarm_test_run_detail')
                ->fields(array(
                  'tri' => $test_run_id,
                  'result' => ($testdetail['result'] == 'true' ? 1 : 0),
                  'message' => $testdetail['message'],
                  'actual' => $testdetail['actual'],
                  'expected' => $testdetail['expected'],
                ))
                ->execute();
            }
          }
          elseif (isset($_REQUEST['log']['default'][$test['name']])) {
            foreach ($_REQUEST['log']['default'][$test['name']] as $testdetail) {
              db_insert('testswarm_test_run_detail')
                ->fields(array(
                  'tri' => $test_run_id,
                  'result' => ($testdetail['result'] == 'true' ? 1 : 0),
                  'message' => $testdetail['message'],
                  'actual' => $testdetail['actual'],
                  'expected' => $testdetail['expected'],
                ))
                ->execute();
            }
          }
        }
      }
    }
  }
}

/**
 * Get a list of all defined tests.
 * @TODO: Add php callback for setup and teardown
 */
function testswarm_defined_tests($module = '') {
  $defaults = array(
    'query' => array(),
    'permissions' => array('run testswarm tests'),
  );

  $tests = module_invoke_all('testswarm_tests');
  foreach ($tests as $caller => &$test) {
    $test['caller'] = $caller;
    $test = array_merge_recursive($defaults, $test);
  }
  return $tests;
}

/**
 * Get a list of all defined tests the current user can do.
 */
function testswarm_defined_tests_access($module = '') {
  $tests = testswarm_defined_tests($module);
  $allowdtests = array();

  foreach ($tests as $caller => &$test) {
    if (testswarm_user_can_run_test($test)) {
      $allowdtests[$caller] = $test;
    }
  }
  return $allowdtests;
}

/**
 * Get a list of all defined tests the current user can do grouped by module.
 */
function testswarm_defined_modules($module = '') {
  $tests = testswarm_defined_tests($module);
  $allowdmodules = array();

  foreach ($tests as $caller => &$test) {
    $allowdmodules[$test['module']][] = $test;
  }
  return $allowdmodules;
}

/**
 * Get a list of all defined tests the current user can do grouped by module.
 */
function testswarm_defined_modules_access($module = '') {
  $tests = testswarm_defined_modules($module);
  $allowdmodules = array();

  foreach ($tests as $caller => &$test) {
    if (testswarm_user_can_run_test($test)) {
      $allowdmodules[$test['module']][] = $test;
    }
  }
  return $allowdmodules;
}

/**
 * Overview of the current user tests.
 * Also used for automated testing.
 * @TODO: Split in at least 2 functions
 */
function testswarm_browser_tests() {

  // Auto refresh page each 60 seconds
  // @TODO: make it a setting
  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'refresh',
      'content' => '300',
    ),
  );
  drupal_add_html_head($element, 'refresh');

  $TESTSWARM_AUTO_MODE = TRUE;

  if (isset($_SESSION['testswarm_tests']) && !empty($_SESSION['testswarm_tests'])) {
    testswarm_run_a_test();
  }

  $output = '';
  $tests = testswarm_defined_tests();
  $allowedtests = testswarm_defined_tests_access();
  $enabled_themes = testswarm_themes_to_test();

  $tests_to_run = array();
  foreach($enabled_themes as $enabled_theme) {
    $tests_to_run[$enabled_theme] = $allowedtests;
  }

  $sql = "SELECT caller, theme, count(caller) as count, total,
      count(caller) as num_runs,
      avg(failed) as failed,
      avg(runtime) as runtime,
      min(timestamp) as first_run, max(timestamp) as last_run
    FROM {testswarm_test} q
    WHERE useragent = :useragent
      AND githash = :githash
    GROUP BY caller, theme
    ORDER BY caller";

  $result = db_query($sql, array(
    ':useragent' => check_plain($_SERVER['HTTP_USER_AGENT']),
    ':githash' => variable_get('testswarm_githash', ''),
  ));

  $browser = get_browser();
  $output .= '<h2>' . $browser->browser . ' (' . $browser->parent . ')' . '</h2>';
  $output .= '<p>' . check_plain($_SERVER['HTTP_USER_AGENT']) . '</p>';
  $output .= '<p>Current drupal version: ' . l(_testswarm_short_githash(), 'http://drupalcode.org/project/drupal.git/commit/' . variable_get('testswarm_githash', '')) . '</p>';
  $output .= '<p>' . l('Run all tests', 'testswarm-run-all-tests') . '</p>';

  $header = array(
    'test',
    'theme',
    '# runs',
    '# tests',
    '# failed',
    'time taken',
    'first run',
    'last run',
  );
  $rows = array();
  foreach ($result as $rowdata) {
    $rows[] = array(
      'data' => array(
        $tests[$rowdata->caller]['module'] . '::' . $rowdata->caller
          . ' | ' . l('Details', 'testswarm-tests/detail/' . $rowdata->caller)
          . (testswarm_user_can_run_test($tests[$rowdata->caller]) ? ' | ' . l('Test now', 'testswarm-run-a-test/' . $rowdata->caller, array('query' => array('destination' => 'testswarm-tests/detail/' . $rowdata->caller))) : '')
          . (testswarm_user_can_run_test($tests[$rowdata->caller]) ? ' | ' . l('Test manually', 'testswarm-run-a-test/' . $rowdata->caller, array('query' => array('destination' => 'testswarm-tests/detail/' . $rowdata->caller, 'debug' => 'on'))) : '')
          . (user_access('administer testswarm tests') ? ' | ' . l('Clear test details', 'testswarm-tests/clear/' . $rowdata->caller, array('query' => array('destination' => 'testswarm-tests/detail/' . $rowdata->caller))) : ''),
        $rowdata->theme,
        $rowdata->num_runs,
        $rowdata->total,
        $rowdata->failed,
        $rowdata->runtime,
        format_date($rowdata->first_run, 'short'),
        format_date($rowdata->last_run, 'short'),
      ),
      'class' => ($rowdata->failed == 0 ? array('testswarm-passed') : array('testswarm-failed')),
    );
    if (array_key_exists($rowdata->caller, $tests_to_run[$rowdata->theme])) {
      unset($tests_to_run[$rowdata->theme][$rowdata->caller]);
      if (empty($tests_to_run[$rowdata->theme])) {
        unset($tests_to_run[$rowdata->theme]);
      }
    }
  }

  if (!empty($tests_to_run)) {
    if ($TESTSWARM_AUTO_MODE) {
      $theme = array_shift(array_keys($tests_to_run));
      $test = array_shift(array_shift($tests_to_run));
      $query = $test['query'];
      if (!is_array($query)) {
        $query = (array)$query;
      }
      $query += array(
        'testswarm-test' => $test['caller'],
        'testswarm-theme' => $theme,
      );
      drupal_goto($test['path'], array('query' => $query));
    }
    else {
      foreach ($tests_to_run as $theme => $tests) {
        foreach ($tests as $test) {
          $row = array(
            'data' => array(
              $test['caller'] . (testswarm_user_can_run_test($test) ? ' ' . l('Test now', 'testswarm-run-a-test/' . $test['caller'], array('query' => array('testswarm-theme' => $theme))) : ''),
              0,
              0,
              0,
              0,
              '-',
              '-',
            ),
            'class' => array('testswarm-untested'),
          );
          array_unshift($rows, $row);
        }
      }
    }
  }

  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));


  drupal_add_css(drupal_get_path('module', 'testswarm') . '/testswarm.css');
  return $output;

}

/**
 * Check permissions to see if the current user can run this test.
 */
function testswarm_user_can_run_test($test) {
  foreach ($test['permissions'] as $permission) {
    if (!user_access($permission)) {
      return FALSE;
    }
  }
  return TRUE;
}

function _testswarm_short_githash($githash = '') {
  if (empty($githash)) {
    $githash = variable_get('testswarm_githash', '');
  }
  return drupal_substr($githash, 0, 7);
}

function testswarm_themes_to_test() {
  $themes = list_themes();
  $enabled_themes = array();

  foreach ($themes as $key => $theme) {
    if ($theme->status == 1) {
      $enabled_themes[] = $key;
    }
  }
  return $enabled_themes;
}

/**
 * Implements hook_custom_theme().
 */
function testswarm_custom_theme() {
  if (isset($_GET['testswarm-theme'])) {
    return $_GET['testswarm-theme'];
  }
}

/**
 * Set karma and redirect.
 */
function testswarm_set_karma($karma) {
  setrawcookie('karma', check_plain($karma), REQUEST_TIME + 31536000, '/');
  drupal_goto('testswarm-run-all-tests');
}

/**
 * Get all karma minus 'Jane Doe'.
 */
function testswarm_get_karmas() {
  $karmas = array();

  $sql = "SELECT karma, count(karma) as count
    FROM {testswarm_test}
    WHERE karma <> '' AND karma <> 'jane doe'
    GROUP BY karma
    ORDER BY karma";
  $result = db_query($sql);

  foreach ($result as $row) {
    $karmas[$row->karma] = $row->count;
  }

  return $karmas;
}

function testswarm_run_browserstack_tests() {
  $url = testswarm_browserstack_url();
  $still_running = drupal_http_request($url . "workers");
  $still_running->data = drupal_json_decode($still_running->data);
  if ($still_running->code != 200) {
    watchdog('TestSwarm Browserstack', t('Error retreiving running tests from browserstack: !message', array('!message' => $still_running->status_message)));
    return;
  }
  elseif (!empty($still_running->data)) {
    watchdog('TestSwarm Browserstack', t('Browserstack is still running tests.'));
    return;
  }
  $response = drupal_http_request($url . "browsers");
  if ($response->code != 200) {
    watchdog('TestSwarm Browserstack', t('Error retreiving browsers from browserstack: !message', array('!message' => $response->status_message)));
    return;
  }
  $browsers = drupal_json_decode($response->data);
  $browser_url = url('testswarm-set-karma/browserstack', array('absolute' => TRUE));
  foreach ($browsers as $browser) {
    $data = "";
    $data .= "browser={$browser['browser']}&version={$browser['version']}&url={$browser_url}&timeout=300";
    $response = drupal_http_request($url . "worker", array('method' => 'POST', 'data' => $data));
    if ($response->code != 200) {
      watchdog('TestSwarm Browserstack', t('Error starting test for browser (!browser): !message', array('!browser' => $browser['browser'] . ' ' . $browser['version'], '!message' => $response->status_message)));
    }
  }
}

function testswarm_delete_browserstack_workers() {
  $workers = testswarm_get_workers();
  $url = testswarm_browserstack_url();
  if ($workers) {
    foreach ($workers->data as $worker) {
      $delete = drupal_http_request($url . "worker/" . $worker['id'], array('method' => 'DELETE'));
      $delete->data = drupal_json_decode($delete->data);
    }
  }
  else {
    drupal_set_message(t('No workers were deleted', 'warning'));
  }
}

function testswarm_get_workers() {
  $url = testswarm_browserstack_url();
  $workers = drupal_http_request($url . "workers");
  if ($workers->code != 200) {
    $message = t('An error occured retreiving workers from browserstack: !message', array('!message' => $workers->status_message));
    watchdog('TestSwarm Browserstack', $message);
    drupal_set_message($message, 'error');
    return FALSE;
  }
  $workers->data = drupal_json_decode($workers->data);
  return $workers;
}

function testswarm_browserstack_url() {
  $username = variable_get('testswarm_browserstack_username', '');
  $password = variable_get('testswarm_browserstack_password', '');
  $url = variable_get('testswarm_browserstack_api_url', 'http://api.browserstack.com/1');
  $url = substr($url, -1) == '/' ? $url : $url . '/';
  $url = str_replace(array('http://', 'https'), "", $url);
  $url = "http://{$username}:{$password}@{$url}";
  return $url;
}